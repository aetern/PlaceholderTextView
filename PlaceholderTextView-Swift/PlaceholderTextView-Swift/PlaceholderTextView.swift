//
//  PlaceholderTextView.swift
//  PlaceholderTextView-Swift
//
//  Created by AeternChan on 8/7/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import UIKit

public class PlaceholderTextView: UITextView {
    
    let kTextKey = "text"
    private var placeholderView = UITextView()
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setUpPlaceholderView()
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
        self.removeObserver(self, forKeyPath: kTextKey)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        placeholderView.frame = self.bounds
    }
    
    func setUpPlaceholderView() {
        placeholderView.editable = false
        placeholderView.scrollEnabled = false
        placeholderView.userInteractionEnabled = false
        placeholderView.textColor = UIColor.lightGrayColor()
        placeholderView.backgroundColor = UIColor.clearColor()
        
        placeholderView.font = self.font
        placeholderView.textAlignment = self.textAlignment
        placeholderView.contentInset = self.contentInset
        placeholderView.contentOffset = self.contentOffset
        placeholderView.textContainerInset = self.textContainerInset
        
        self.addSubview(placeholderView)
        
        var defaultCenter = NSNotificationCenter.defaultCenter()
        defaultCenter.addObserver(self, selector: "textDidChange:", name: UITextViewTextDidChangeNotification, object: self)
        self.addObserver(self, forKeyPath: kTextKey, options: .New, context: nil)
    }
    
    override public func observeValueForKeyPath(keyPath: String, ofObject object: AnyObject, change: [NSObject : AnyObject], context: UnsafeMutablePointer<Void>) {
        if keyPath == kTextKey {
            placeholderView.hidden = self.hasText()
        } else {
            super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
        }
    }
    
    func textDidChange(notification: NSNotification) {
        placeholderView.hidden = self.hasText()
    }
    
    
    // MARK: - Property Accessor
    
    override public var font: UIFont! {
        set(newValue) {
            super.font = newValue
            placeholderView.font = newValue
        }
        get {
            return super.font
        }
    }
    
    override public var textAlignment: NSTextAlignment {
        set(newValue) {
            super.textAlignment = newValue
            placeholderView.textAlignment = newValue
        }
        get {
            return super.textAlignment
        }
    }
    
    override public var contentInset: UIEdgeInsets {
        set(newValue) {
            super.contentInset = newValue
            placeholderView.contentInset = newValue
        }
        get {
            return super.contentInset
        }
    }
    
    override public var contentOffset: CGPoint {
        set(newValue) {
            super.contentOffset = newValue
            placeholderView.contentOffset = newValue
        }
        get {
            return super.contentOffset
        }
    }
    
    override public var textContainerInset: UIEdgeInsets {
        set(newValue) {
            super.textContainerInset = newValue
            placeholderView.textContainerInset = newValue
        }
        get {
            return super.textContainerInset
        }
    }
    
    
    // MARK: - Placeholder Accessor
    
    public var placeholder: String {
        set(newValue) {
            placeholderView.text = newValue
        }
        get {
            return placeholderView.text
        }
    }
}
