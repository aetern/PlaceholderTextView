//
//  PlaceholderTextView.h
//  PlaceholderTextView-OC
//
//  Created by AeternChan on 8/7/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaceholderTextView : UITextView

@property (nonatomic, strong) NSString *placeholder;

@end
